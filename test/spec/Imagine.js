describe("Imagine", function () {

  beforeEach(function () {
    imagine = new Imagine();
  });

  describe('#create', function () {
    it("creates a new Image object using a callback", function () {
      imagine.create("", function (err, image) {
        expect(err).not.toBeTruthy();
        expect(image.src).toBeDefined();
      });
    });
  });

});
