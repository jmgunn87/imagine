function Imagine() {}

Imagine.prototype = {

  create: function (representation, callback) {
    var image = new Image();
    image.onload = function () {
      callback.call(this, false, this);
    };
    image.onerror = function () {
      callback.call(this, true, this);
    };

    if (typeof representation === 'object') {
      var reader = new FileReader();
      reader.onload = function (e) {
        image.src = e.target.result;
      };
      reader.readAsDataURL(representation);
    } else {
      image.src = representation;
    }

    return image;
  }

};