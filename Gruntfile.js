module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: {
        src: ['./src/**/*.js']
      }
    },
    concat: {
      options: {
        banner: ';(function () { \n\n  "use strict";\n\n',
        seperator: "\n",
        footer: '\n\n  window.Imagine = Imagine;\n\n})();',
      },
      dev: {
        src: ['src/Imagine.js'],
        dest: './imagine.js'
      }
    },
    jasmine: {
      all: {
        src: ['src/Imagine.js'],
        options: {
          specs: './test/spec/**/*.js'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        report: 'gzip'
      },
      all: {
        files: {
          './imagine.min.js': ['./imagine.js']
        }
      }
    }
 });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('test', [
    'jshint',
    'concat',
    'jasmine'
  ]);
  grunt.registerTask('build', [
    'jshint',
    'concat',
    'jasmine',
    'uglify'
  ]);
};